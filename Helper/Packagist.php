<?php

namespace Wizkunde\Packagist\Helper;

use Magento\Framework\Exception\InputException;
use Symfony\Component\Config\Definition\Exception\Exception;
use PrivatePackagist\ApiClient\Exception\ResourceNotFoundException;

class Packagist extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $scopeConfig;
    protected $client;

    /**
     * Request constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \PrivatePackagist\ApiClient\Client $packagistClient
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \PrivatePackagist\ApiClient\Client $packagistClient)
    {
        parent::__construct($context);

        $this->scopeConfig = $context->getScopeConfig();
        $this->client = $packagistClient;

        $this->client->authenticate($this->getToken(), $this->getSecret());
    }

    public function getToken()
    {
        return $this->scopeConfig->getValue('packagist/general/token', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getSecret()
    {
        return $this->scopeConfig->getValue('packagist/general/secret', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @return string
     */
    protected function getCustomerUrlName(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        $prefix = $this->scopeConfig->getValue('packagist/general/prefix', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return trim(preg_replace('/\W+/', '-', strtolower($prefix))) . '-' . $order->getCustomerId();
    }

    /**
     * @param \Magento\Customer\Api\Data\CustomerInterface $customer
     * @return string
     */
    protected function getCustomerUrlNameByCustomer(\Magento\Customer\Api\Data\CustomerInterface $customer)
    {
        $prefix = $this->scopeConfig->getValue('packagist/general/prefix', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return trim(preg_replace('/\W+/', '-', strtolower($prefix))) . '-' . $customer->getId();
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @return mixed
     */
    public function getVendor(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        if($order->getCustomerIsGuest() == true) {
            return false;
        }

        try {
            $customer = $this->client->customers()->show($this->getCustomerUrlName($order));
        } catch (ResourceNotFoundException $resourceNotFound) {
            $prefix = $this->scopeConfig->getValue('packagist/general/prefix', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $customer = $this->client->customers()->create(ucwords($prefix) . ' ' . $order->getCustomerId(), false, $this->getCustomerUrlName($order));
        }

        return $customer;
    }

    /**
     * @param \Magento\Customer\Api\Data\CustomerInterface $customer
     * @return mixed
     */
    public function getVendorByCustomer(\Magento\Customer\Api\Data\CustomerInterface $customer)
    {
        $this->client->authenticate($this->getToken(), $this->getSecret());

        try {
            $customer = $this->client->customers()->show($this->getCustomerUrlNameByCustomer($customer));
        } catch (ResourceNotFoundException $resourceNotFound) {
            $prefix = $this->scopeConfig->getValue('packagist/general/prefix', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $customer = $this->client->customers()->create(ucwords($prefix) . ' ' . $customer->getId(), false, $this->getCustomerUrlNameByCustomer($customer));
        }

        return $customer;
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @return bool
     */
    public function addPackage(\Magento\Sales\Api\Data\OrderInterface $order, \Magento\Catalog\Api\Data\ProductInterface $product)
    {
        if($order->getCustomerIsGuest() == true || $product->getPackagistPackage() == null) {
            return false;
        }

        $customer = $this->getVendor($order);

        $packages = $this->client->customers()->listPackages($customer['id']);

        if(is_array($packages) && count($packages) > 0) {
            foreach($packages as $package)
            {
                if($package['name'] == $product->getPackagistPackage())
                {
                    return true;
                }
            }
        }

        $this->client->customers()->addOrEditPackages($customer['id'], array(array('name' => $product->getPackagistPackage())));

        return true;
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @param $packageName
     * @return mixed
     */
    public function getPackage(\Magento\Sales\Api\Data\OrderInterface $order, $packageName)
    {
        $packages = $this->getPackages($order);

        if(is_array($packages) && count($packages) > 0) {
            foreach($packages as $package)
            {
                if($package['name'] == $packageName)
                {
                    return $package;
                }
            }
        }

        return false;
    }

    /**
     * @param $packageName
     * @return mixed
     */
    public function getPackageInfo($packageName)
    {
        return $this->client->packages()->show($packageName);
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @return mixed
     */
    public function getPackages(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        if($order->getCustomerIsGuest() == true) {
            return false;
        }

        $customer = $this->getVendor($order);

        return $this->client->customers()->listPackages($customer['id']);
    }

    /**
     * @param \Magento\Customer\Api\Data\CustomerInterface $customer
     * @return mixed
     */
    public function getPackagesByCustomer(\Magento\Customer\Api\Data\CustomerInterface $customer)
    {
        $customer = $this->getVendorByCustomer($customer);
        return $this->client->customers()->listPackages($customer['id']);
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @return bool|null
     */
    public function revokePackage(\Magento\Sales\Api\Data\OrderInterface $order, \Magento\Catalog\Api\Data\ProductInterface $product)
    {
        if($order->getCustomerIsGuest() == true || $product->getPackagistPackage() == null) {
            return false;
        }

        $customer = $this->getVendor($order);

        $packages = $this->client->customers()->listPackages($customer['id']);

        if(is_array($packages) && count($packages) > 0) {
            foreach($packages as $package)
            {
                if($package['name'] == $product->getPackagistPackage())
                {
                    $this->client->customers()->removePackage($customer['id'], $product->getPackagistPackage());
                    return true;
                }
            }
        }

        return false;
    }
}
