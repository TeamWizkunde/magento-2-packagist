<?php

namespace Wizkunde\Packagist\Plugin;

class RevokePackage
{
    protected $orderRepository;
    protected $packagistHelper;
    protected $productRepository;

    /**
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Wizkunde\Packagist\Helper\Packagist $packagistHelper
     */
    public function __construct(
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Wizkunde\Packagist\Helper\Packagist $packagistHelper
    ) {
        $this->orderRepository = $orderRepository;
        $this->packagistHelper = $packagistHelper;
        $this->productRepository = $productRepository;
    }

    /**
     * @param Magento\Sales\Model\Service\CreditmemoService $subject
     * @param \Magento\Sales\Api\Data\CreditmemoInterface $creditmemo
     * @return \Magento\Sales\Api\Data\CreditmemoInterface
     */
    public function afterRefund($subject, \Magento\Sales\Api\Data\CreditmemoInterface $creditmemo)
    {
        if($creditmemo->getOrderId() != null)
        {
            $order = $this->orderRepository->get($creditmemo->getOrderId());

            foreach($order->getAllItems() as $item) {
                if($item->getProductType() == 'downloadable') {
                    $this->packagistHelper->revokePackage($order, $this->productRepository->getById($item->getProductId()));
                }
            }

            return $creditmemo;
        }

    }
}