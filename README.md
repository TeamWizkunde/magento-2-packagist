[TOC]

# About the Extension
This extension provides a direct integration between Magento 2 and Private Packagist.
Once this extension is installed, you can specify the packagist configuration at each downloadable product. 

## After placing an order
* The extension creates a vendor when the customer first purchases an downloadable product of your website
* The package is made accessible by the extension automatically when a purchase is invoiced

NOTE: Only orders created by registered customers will create Packagist credentials, guest orders will not work.

## After refunding an order
* The extension revokes the access to the package when a Creditmemo is created inside Magento 2

* * *

# Installation
composer require wizkunde/magento2-packagist

Proceed with your Magento 2 Upgrade steps, which are atleast:
* bin/magento setup:upgrade
* bin/magento cache:flush

* * *

# Configuration

## Step 1: Create API Credentials at Private Packagist

* Go to your Private Packagist Account and go to the Organization you want to make accessible.
* Go to Settings -> API Access
* Click "Create a new set of API credentials"
* Store the token and the secret temporarily, you'll need them shortly

## Step 2: Configure your Magento 2 Installation

* Go to your Magento 2 admin panel
* Go to: Stores -> Configuration -> General -> Wizkunde Packagist Configuration
* Unfold the Private Packagist tab
* Enter your Token in the "API Token" field
* Enter your Secret in the "API Secret" field
* Click "Save Config"

Optionally, you can set a specific prefix, if you plan to use different websites on a single integration

## Step 3: Connect a Downloadable product to Private Packagist

* Navigate to Catalog -> Products
* Open up a Downloadable product which is also available in your Private Packagist Organization
* Unfold "Packagist"
* Set the packagist extension key in "Packagist Package"

You're done! When a customer purchases the extension, he will get instructions to install with packagist instead!
These instructions can be found directly on the account dashboard