<?php
namespace Wizkunde\Packagist\Block\Composer;

use Magento\Framework\Exception\NoSuchEntityException;

class Instructions extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Customer\Helper\View
     */
    protected $_helperView;

    /**
     * @var \Magento\Customer\Helper\Session\CurrentCustomer
     */
    protected $currentCustomer;

    protected $packagistHelper;

    protected $productCollectionFactory;

    protected $orderRepository;
    protected $orderCollectionFactory;
    protected $productRepository;

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer
     * @param \Magento\Customer\Helper\View $helperView
     * @param \Wizkunde\Packagist\Helper\Packagist $packagistHelper
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer,
        \Magento\Customer\Helper\View $helperView,
        \Wizkunde\Packagist\Helper\Packagist $packagistHelper,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        array $data = []
    ) {
        $this->currentCustomer = $currentCustomer;
        $this->_helperView = $helperView;
        $this->packagistHelper = $packagistHelper;
        $this->productCollectionFactory = $productCollectionFactory;

        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->orderRepository = $orderRepository;
        $this->productRepository = $productRepository;

        parent::__construct($context, $data);
    }

    /**
     * Returns the Magento Customer Model for this block
     *
     * @return \Magento\Customer\Api\Data\CustomerInterface|null
     */
    public function getCustomer()
    {
        try {
            return $this->currentCustomer->getCustomer();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    public function getVendor()
    {
        if($this->getCustomer() != null)
        {
            return $this->packagistHelper->getVendorByCustomer($this->getCustomer());
        }

        return null;
    }

    /**
     * @return string
     */
    public function getVendorToken()
    {
        $vendor = $this->getVendor();

        if($vendor != null) {
            return $vendor['composerRepository']['token'];
        }

        return '';
    }

    /**
     * @return string
     */
    public function getVendorUrl()
    {
        $vendor = $this->getVendor();

        if($vendor != null) {
            return $vendor['composerRepository']['url'];
        }

        return '';
    }

    /**
     * @return mixed
     */
    public function getVendorUrlName()
    {
        $vendor = $this->getVendor();
        return $vendor['urlName'];
    }

    /**
     * @return mixed
     */
    public function getVendorHost()
    {
        $urlData = parse_url($this->getVendorUrl());
        return $urlData['host'];
    }

    public function getPackages()
    {
        if($this->getCustomer() != null)
        {
            // Lookup first to see if we got it
            $packages = $this->packagistHelper->getPackagesByCustomer($this->getCustomer());

            $orderCollection = $this->orderCollectionFactory->create()->addAttributeToFilter('customer_email', $this->getCustomer()->getEmail());

            foreach($orderCollection as $orderItem) {
                $order = $this->orderRepository->get($orderItem->getId());

                foreach($order->getAllItems() as $item) {
                    if($item->getProductType() == 'downloadable') {
                        $product = $this->productRepository->getById($item->getProductId());

                        if(in_array($product->getPackagistPackage(), array_column($packages, 'name')) === false) {
                            $this->packagistHelper->addPackage($order, $this->productRepository->getById($item->getProductId()));
                            $packages = $this->packagistHelper->getPackagesByCustomer($this->getCustomer());
                        }
                    }
                }
            }

            return $packages;
        }

        return null;
    }

    /**
     * @param $packageName
     * @return mixed|null
     */
    public function getPackageInfo($packageName)
    {
        if($this->getCustomer() != null)
        {
            return $this->packagistHelper->getPackageInfo($packageName);
        }

        return null;
    }

    /**
     * @param $packageName
     * @return mixed|null
     */
    public function getVersions($packageName)
    {
        if($this->getCustomer() != null)
        {
            $package = $this->packagistHelper->getPackageInfo($packageName);

            $versions = array();

            foreach($package['versions'] as $version) {
                if($version['version'] != 'dev-master') {
                    $versions[] = $version['version'];
                }
            }

            usort($versions, 'version_compare');


            return array_reverse($versions);
        }

        return array();
    }

    /**
     * Get the full name of a customer
     *
     * @return string full name
     */
    public function getName()
    {
        return $this->_helperView->getCustomerName($this->getCustomer());
    }
}
