<?php
namespace Wizkunde\Packagist\Block\Composer;

use Magento\Framework\Exception\NoSuchEntityException;

class Product extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Customer\Helper\Session\CurrentCustomer
     */
    protected $currentCustomer;

    protected $coreRegistry;

    protected $packagistHelper;

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Wizkunde\Packagist\Helper\Packagist $packagistHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer,
        \Magento\Framework\Registry $coreRegistry,
        \Wizkunde\Packagist\Helper\Packagist $packagistHelper,
        array $data = []
    ) {
        $this->currentCustomer = $currentCustomer;
        $this->packagistHelper = $packagistHelper;

        $this->coreRegistry = $coreRegistry;

        parent::__construct($context, $data);
    }

    /**
     * Returns the Magento Customer Model for this block
     *
     * @return \Magento\Customer\Api\Data\CustomerInterface|null
     */
    public function getCustomer()
    {
        try {
            return $this->currentCustomer->getCustomer();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * @return mixed|null
     */
    public function getPackageInfo()
    {
        if ($this->coreRegistry->registry('product') !== null) {
            $product = $this->coreRegistry->registry('product');

            if ($product->getPackagistPackage() != null) {
                $packageData = $this->packagistHelper->getPackageInfo($product->getPackagistPackage());

                return [
                    'name' => $product->getPackagistPackage(),
                    'latest' => $this->getLatestVersion($packageData)

                ];
            }
        }

        return null;
    }

    /**
     * @param $packageData
     * @return mixed|null
     */
    public function getLatestVersion($packageData)
    {
        $versions = [];

        foreach ($packageData['versions'] as $version) {
            if ($version['version'] != 'dev-master') {
                $versions[] = $version['version'];
            }
        }

        if (count($versions) > 0) {
            usort($versions, 'version_compare');
            return current(array_reverse($versions));
        }

        return 'dev-master';
    }
}
